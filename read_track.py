#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

#   U
# L · R
#   D

U = 0
R = 1
D = 2
L = 3

rel_pos = {
    U: (0, -1),
    D: (0, +1),
    R: (+1, 0),
    L: (-1, 0)
}


FIRST = '┅'


class Block:
    def __init__(self, *ends):
        self.ends = ends

    def other(self, end):
        end = {L: R, R: L, U: D, D: U}[end]
        ends = list(self.ends)
        ends.remove(end)
        return ends[0]

    def __repr__(self):
        return self.ends


block_map = {
    '╭': Block(R, D),
    '╮': Block(L, D),
    '╰': Block(U, R),
    '╯': Block(L, U),
    '│': Block(U, D),
    '─': Block(L, R),
    '┅': Block(L, R),
}


class Track:
    def __init__(self, data):
        print(data)
        self.blocks = data
        self.heigth = len(self.blocks)
        self.width = len(self.blocks[0])
        # FIXME: assert all rows have same length

    def find_first(self):
        for y, row in enumerate(self.blocks):
            for x, b in enumerate(row):
                if b == FIRST:
                    return x, y

    @staticmethod
    def add_pos(pos1, pos2):
        return pos1[0] + pos2[0], pos1[1] + pos2[1]

    def get_next_pos(self, pos, end):
        return self.add_pos(pos, rel_pos[end])

    def iterate(self):
        first = pos = self.find_first()
        yield first

        next_end = R
        while 1:
            pos = self.get_next_pos(pos, next_end)
            if pos == first:
                raise StopIteration

            yield pos
            char = self.blocks[pos[1]][pos[0]]
            next_end = block_map[char].other(next_end)


with open("01.track") as fd:
    track = Track(fd.read().splitlines())

print("circuit size is: {} x {}".format(track.width, track.heigth))

for i in track.iterate():
    print(i)
