# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase


from read_track import block_map, U, D, R, L


class BlockTests(TestCase):
    def test_other(self):
        b = block_map['╭']
        self.assertEquals(b.other(R), D)
